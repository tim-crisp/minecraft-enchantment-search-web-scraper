const puppeteer = require('puppeteer');
const R = require('ramda');
const fs = require('fs').promises;

const getEnchantmentItemMapping = R.compose(
  R.reduce(R.mergeWith(R.concat), {}),
  R.flatten,
  R.map(
    R.compose(
      R.apply(R.map),
      R.juxt([
        R.compose(fn => R.compose(fn, R.of), R.objOf, R.prop('name')),
        R.compose(
          R.pluck('title'),
          R.prop('primaryItems')
        )
      ])
    )
  )
);

const getEnchantmentItemMappingv2 = R.compose(
  R.reduceBy((acc, [item]) => acc.concat(item), [], R.nth(1)),
  R.unnest,
  R.map(
    R.converge(R.xprod, [
      R.compose(R.pluck('title'), R.prop('primaryItems')),
      R.compose(R.of, R.prop('name')),
    ])
  )
);

const main = async () => {

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  // await page.goto('https://example.com');
  // await page.screenshot({ path: 'example.png' });

  // const htmlString = await fs.readFile('./minecraft_enchanting.html');
  // await page.goto(`data:text/html,${html}`, { waitUntil: 'networkidle2' });


  // await page.goto(`data:text/html;data:text/html;charset=utf-8,%3C%21DOCTYPE%20html%3E%0D%0A%3Chtml%3E%0D%0A%20%20%3Chead%3E%0D%0A%20%20%20%20%3Ctitle%3ESomething%3C%2Ftitle%3E%0D%0A%20%20%20%20%3Cscript%3E%0D%0A%20%20%20%20%20%20console.log%28window.document%29%3B%0D%0A%20%20%20%20%20%20console.log%28%27test%27%2C%20document.querySelector%28%27%23app%27%29%29%3B%0D%0A%20%20%20%20%20%20window.onload%20%3D%20%28%29%20%3D%3E%20%7B%0D%0A%20%20%20%20%20%20%20%20console.log%28%27done%27%29%3B%0D%0A%20%20%20%20%20%20%20%20console.log%28document.querySelector%28%27%23app%27%29%29%3B%0D%0A%20%20%20%20%20%20%20%20setTimeout%28%28%29%20%3D%3E%20%7B%0D%0A%20%20%20%20%20%20%20%20%20%20const%20newElement%20%3D%20document.createElement%28%27div%27%29%3B%0D%0A%20%20%20%20%20%20%20%20%20%20newElement.setAttribute%28%27id%27%2C%20%27im-here%27%29%3B%0D%0A%20%20%20%20%20%20%20%20%20%20newElement.innerText%20%3D%20%27Hello%20world%21%27%3B%0D%0A%20%20%20%20%20%20%20%20%20%20%0D%0A%20%20%20%20%20%20%20%20%20%20document.querySelector%28%27%23app%27%29.appendChild%28%0D%0A%20%20%20%20%20%20%20%20%20%20%20%20newElement%0D%0A%20%20%20%20%20%20%20%20%20%20%29%3B%0D%0A%20%20%20%20%20%20%20%20%7D%2C%205000%29%3B%0D%0A%20%20%20%20%20%20%7D%0D%0A%20%20%20%20%3C%2Fscript%3E%0D%0A%20%20%3C%2Fhead%3E%0D%0A%20%20%3Cbody%3E%0D%0A%20%20%20%20%3Cdiv%20id%3D%22app%22%3E%0D%0A%20%20%20%20%20%20%0D%0A%20%20%20%20%3C%2Fdiv%3E%0D%0A%20%20%3C%2Fbody%3E%0D%0A%3C%2Fhtml%3E`);

  // console.log('navigated');

  // console.time('duration');

  // const divHandle = await page.waitFor('#im-here');
  // // const handle = await page.$('#im-here');
  // const text = await page.evaluate(content => content.innerText, divHandle);

  // console.log(text);
  // // await handle.dispose();
  // await divHandle.dispose();

  // console.log('we got it!')
  // console.timeEnd('duration');

  // return;

  await page.goto(`https://minecraft.gamepedia.com/Item_Frame`);

  const craftingHandle = await page.waitFor('[data-description="Crafting recipes"]');
  const textContent = await page.evaluate(craftingTable => {

    let inventorySlots = [...craftingTable.querySelector('.mcui.mcui-Crafting_Table').querySelectorAll('.invslot')];

    if (inventorySlots.length > 0) {
      if (inventorySlots.length === 10) {
        let craftingResult = inventorySlots.pop();
        result = inventorySlots[3];

        let craftingItems = inventorySlots.map(item => {
          const itemInv = [...item.querySelectorAll('.invslot-item')];

          const items = itemInv.map(inventorySlotItem => {
            let title = inventorySlotItem.getAttribute('data-minetip-title');
            if (!title) {
              title = inventorySlotItem.querySelector('a').title;
            }
            const imageSpan = inventorySlotItem.querySelector('span');
            const image = imageSpan.style.backgroundImage;
            const [x, y] = imageSpan.style.backgroundPosition.replace(/px/g, '').split(' ');
            const link = inventorySlotItem.querySelector('a').href;
            return { title, image: { image, x, y }, link };
          });
          return items;
        });
        result = craftingItems;
        return result;
        console.log(inventorySlots.length);
      } else if (inventorySlots.length === 5) {

      } else {
        console.log('unknown');
      }
    }
  }, craftingHandle);

  await craftingHandle.dispose();

  await fs.writeFile('./item.json', JSON.stringify(textContent, null, 2));
  // console.log(textContent);


  await page.goto(`https://minecraft.gamepedia.com/Enchanting`);

  // await page.waitForSelector('[data-description="Summary of enchantments"] tbody > tr');
  const tableHandle = await page.waitFor('[data-description="Summary of enchantments"]');

  try {
    const enchanting = await page.evaluate(table => {
      if (!table) {
        return 'no table';
      }
      const levelMapping = {
        I: 1,
        II: 2,
        III: 3,
        IV: 4,
        V: 5
      };

      const tableRows = table.querySelectorAll('tr');
      console.log(tableRows.length);
      const rows = [...tableRows];
      rows.shift(); // remove the first row
      // return table.innerText;

      const items = rows.map(row => {
        try {
          let [name, summary, maxLvl, primaryItems, secondaryItems, weight] = [...row.querySelectorAll('td')];
          result = primaryItems;

          let link = name.querySelector('a');
          name = link.innerText;
          link = link.href;
          summary = summary.innerText;
          primaryItems = [...primaryItems.querySelectorAll('a')].map(primaryItem => ({ title: primaryItem.title, link: primaryItem.href }));
          secondaryItems = [...secondaryItems.querySelectorAll('a')].map(secondaryItem => ({ title: secondaryItem.title, link: secondaryItem.href }));
          maxLvl = levelMapping[maxLvl.innerText];

          return {
            name,
            link,
            summary,
            maxLvl,
            primaryItems,
            secondaryItems,
          };
        } catch(e) {
          return {
            error: true,
            html: row.innerHTML,
          }
        }
      });
      return items;
    }, tableHandle);

    await tableHandle.dispose();

    const hasError = enchanting.findIndex(element => element && element.error) !== -1;
    console.log(enchanting.length);
    console.log(hasError ? 'We have an error' : 'There is no error');
    await fs.writeFile('./enchanting_table.json', JSON.stringify(enchanting, null, 2));
    await fs.writeFile('./enchanting_item_mapping.json', JSON.stringify(getEnchantmentItemMappingv2(enchanting), null, 2));
  } catch (e) {
    console.log(e);
  }

  await browser.close();
};

main();
