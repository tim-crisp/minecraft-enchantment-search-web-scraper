const R = require('ramda');

const itemInfoTask = async({ page, data: item }) => {
  // console.log(item.link);
  await page.goto(item.link);

  let {
    craftingRecipe,
    craftingResultItems,
  } = await page.evaluate(() => {

    // declare inside the scope of the evaluate to ensure browser is aware
    const extractItemInformation = inventorySlotItem => {
      try {
        let title = inventorySlotItem.getAttribute('data-minetip-title')
          || (inventorySlotItem.querySelector('a') || {}).title
          || (inventorySlotItem.querySelector('.inv-sprite') || {}).title;
        const imageSpan = inventorySlotItem.querySelector('span');
        const image = imageSpan.style.backgroundImage;
        const [x, y] = imageSpan.style.backgroundPosition.replace(/px/g, '').split(' ');
        const link = (inventorySlotItem.querySelector('a') || {}).href || `https://minecraft.gamepedia.com/${title}`;
        return { title, image: { image, x, y }, link };
      } catch (e) {
        return { e: e.message, error: true, text: inventorySlotItem.innerText, html: inventorySlotItem.innerHTML };
      }
    };

    try {
      let inventorySlotElements = [...document.querySelector('.mcui.mcui-Crafting_Table').querySelectorAll('.invslot')];

      // Extract crafting result items

      let craftingResultElement = inventorySlotElements.pop(); // last item is the crafting result

      const craftingResults = [...craftingResultElement.querySelectorAll('.invslot-item')];

      let craftingResultItems = craftingResults.map(extractItemInformation);

      // Extract the items required to craft the result

      const craftingRecipe = inventorySlotElements.map(inventorySlotElement => {
        const children = [...inventorySlotElement.children];
        return children.map(inventorySlotSubItem => {
          if (inventorySlotSubItem.className.indexOf('animated-subframe') != -1) {
            const subInventorySlotElements = [...inventorySlotSubItem.querySelectorAll('.invslot-item')];
            return subInventorySlotElements.map(subInventorySlotElement => {
              return extractItemInformation(subInventorySlotElement);
            });
          } else {
            return extractItemInformation(inventorySlotSubItem);
          }
        });
      });

      return {
        craftingRecipe,
        craftingResultItems,
      };
    } catch (e) {
      return {};
    }
  });

  let parentType;

  try {
    const {
      redirectTitle,
      mainTitle,
    } = await page.evaluate(() => {
      let redirectTitle;
      try {
        const redirectedLink = document.querySelector('.mw-redirectedfrom a');
        redirectTitle = redirectedLink.getAttribute('title') || redirectedLink.innerText;
      } catch (e) {}
      let mainTitle;
      try {
        mainTitle = document.querySelector('#firstHeading').innerText;
      } catch (e) {}
      return {
        redirectTitle,
        mainTitle,
      };
    });
    // console.log(redirectTitle);
    // console.log(mainTitle);

    // // craftingRecipe,
    // // craftingResultItems,
    // const itemIndex = R.findIndex(
    //   R.propEq('title', redirectTitle),
    //   craftingResultItems
    // );
    // console.log(itemIndex);
    
    if (redirectTitle) {
      parentType = mainTitle;
    }
  } catch (e) { }

  
  const paragraph = await page.evaluate(() => {
    try {
      return document.querySelector('#mw-content-text p:not(:first-child)').innerText;
    } catch (e) {
      return null;
    }
  });

  console.log(`title: ${item.title}, parent: ${parentType}`);

  return {
    ...item,
    parentType,
    craftingRecipe,
    craftingResultItems,
    paragraph,
  };
};

const hasOnly3Errors = R.compose(
  R.propEq('length', 3),
  R.filter(R.prop('error'))
);

const getAllSuccessful = R.reject(R.prop('error'));

module.exports = {
  itemInfoTask,
  extractAllItemInfo: async(browser, items) => {
    try {
      const processItem = item => browser.execute(item, itemInfoTask);

      const results = await Promise.all([
        ...items.map(processItem),
      ]);

      // const results = [];
      // let count = 0;
      // const total = items.length;
      // for (let item of items) {
      //   // console.log(`processing ${item.title}`);
      //   results.push(
      //     await processItem(item)
      //   );
      //   console.log(results[results.length-1]);
      //   console.log(`[${++count}/${total}] ${item.title}`);
      // }
      
      return results;

    } catch (e) {
      console.log(e);
      console.log('error');
    }
  },
};
