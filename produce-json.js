const R = require('ramda');
const fs = require('fs');

const enchantingItemMapping = JSON.parse(fs.readFileSync('./enchanting_item_mapping.json'));
const items = JSON.parse(fs.readFileSync('items.json'));

const itemsWithEnchantments = R.compose(
  R.uniq,
  R.flatten,
  R.values,
)(enchantingItemMapping);

const groupByParent = R.groupBy(
  R.ifElse(
    R.has('parentType'),
    R.prop('parentType'),
    R.prop('title')
  )
);

// for now we only care about the items that are craftable

const setTitleFromId = R.mapObjIndexed(
  (value, key) => ({ title: key, ...value })
);

function integeriseImageCoordinates() {
  return R.cond([
    [
      R.is(Array),
      R.map(integeriseImageCoordinates),
    ],
    [
      R.is(Object),
      R.compose(
        R.over(R.lensPath(['image', 'x']), Number),
        R.over(R.lensPath(['image', 'y']), Number),
      ),
    ],
    [R.T, R.identity],
  ])(...arguments);
}

const normaliseItem = R.compose(
  R.dissoc('image'), // the craftingResultItem has the image information
  R.dissoc('craftingResultItems')
);

const extractItemSpecific = item => {
  let { title, craftingResultItems, craftingRecipe } = item;
  if (!craftingRecipe || !craftingResultItems) return item;
  const indexOfItem = R.findIndex(
    R.propEq('title', title),
    craftingResultItems,
  );
  craftingRecipe = R.map(
    R.compose(
      // integeriseImageCoordinates,
      // if crafting recipe entry doesn't exist for each item, assume its universal
      itemSlot => itemSlot.length === craftingResultItems.length ? itemSlot[indexOfItem] : itemSlot[0],
    ),
    craftingRecipe,
  );
  return {
    ...normaliseItem(item),
    craftingResultItem: /*integeriseImageCoordinates*/(craftingResultItems[indexOfItem]),
    craftingRecipe,
  };
};

const selectedItems = R.compose(
  R.values,
  // setTitleFromId,
  // R.map(
  //   R.compose(
  //     R.dissoc('parentType'),
  //     R.dissoc('title'),
  //     R.dissoc('link'),
  //     R.dissoc('image'),
  //     R.head
  //   )
  // ),
  // groupByParent,
  R.map(extractItemSpecific),
  R.filter(
    R.anyPass([
      R.propSatisfies(R.contains(R.__, itemsWithEnchantments), 'title'),
      R.propSatisfies(R.contains(R.__, itemsWithEnchantments), 'parentType'),
    ])
  )
)(items);

console.log(R.flatten(R.values(selectedItems)).length);


fs.writeFileSync('./produced_items.json', JSON.stringify(selectedItems, null, 2));
