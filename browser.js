
const { Cluster } = require('puppeteer-cluster');

module.exports = async () => {
  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_PAGE,
    maxConcurrency: 8,
    puppeteerOptions: {
      // executablePath: '/usr/bin/chromium-browser',
    }
  });
  return {
    cluster,
    execute: async (data, taskFunction) => {
      while (true) {
        try {
          const result = await cluster.execute(data, taskFunction);
          return result;
        } catch (e) { }
      }
    },
    close: async () => {
      await cluster.idle();
      await cluster.close();
    }
  }
};
