
const R = require('ramda');
const fs = require('fs').promises;
const createBrowser = require('./browser');
const { itemInfoTask } = require('./item-info');

const fsNonPromise = require('fs');

const item = {
  link: fsNonPromise.readFileSync('./item-content.html').toString(),
};

(async() => {
  let browser;
  try {
    browser = await createBrowser();

    const result = await browser.execute(item, itemInfoTask);

    let itemWithoutLink = R.dissoc('link', result);

    await fs.writeFile('./single-item.json', JSON.stringify(itemWithoutLink, null, 2));

  } catch (e) {
    console.log(e);
    console.log('Could not start browser');
  } finally {
    if (browser) {
      await browser.close();
    }
  }
})();