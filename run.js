const fs = require('fs').promises;

const createBrowser = require('./browser');

const extractItemList = require('./item-list');
const { extractAllItemInfo } = require('./item-info');

(async() => {
  let browser;
  try {
    browser = await createBrowser();

    // items
    // - crafting recipes

    const items = await extractItemList(browser);
    const itemss = await extractAllItemInfo(browser, items);

    await fs.writeFile('./items.json', JSON.stringify(itemss, null, 2));
    // blocks


  } catch (e) {
    console.log(e);
    console.log('Could not start browser');
  } finally {
    if (browser) {
      await browser.close();
    }
  }
})();
