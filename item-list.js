const R = require('ramda');

const itemListTask = async({ page, data: url }) => {
  await page.goto(url);
  return page.$$eval('.wikitable.stikitable.sortable tr', rows => {
    return [...rows].map(row => {
      try {
        const item = row.querySelector('a');
        const link = item.href;
        const title = item.innerText;
        const itemImage = row.querySelector('.sprite');
        const image = itemImage.style.backgroundImage;
        const [x, y] = itemImage.style.backgroundPosition.replace(/px/g, '').split(' ');
        return {
          title,
          link,
          image: {
            image,
            x,
            y,
          },
        };
      } catch (e) {
        return {
          error: true,
          text: row.innerText,
          html: row.innerHTML,
        };
      }
    });
  });
};

const hasOnly3Errors = R.compose(
  R.propEq('length', 3),
  R.filter(R.prop('error'))
);

const getAllSuccessful = R.reject(R.prop('error'));

module.exports = async(browser) => {
  try {
    const result = await browser.execute('https://minecraft.gamepedia.com/Item', itemListTask);

    if (hasOnly3Errors(result)) {
      const successfulItems = getAllSuccessful(result);
      console.log(successfulItems);
      console.log(successfulItems.length);
      return successfulItems;
    } else {
      throw new Error('Could note get items');
    }

  } catch (e) {
    console.log(e);
    console.log('error');
  }
};
